﻿// BehaviorPatterns.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "Command.h"
#include "Observer.h"
#include "State.h"
#include "Visitor.h"
#include <thread>
#include <chrono>
#include <vector>

void TestCommandPattern()
{
	Tank* tank = new Tank;
	TargetPoint t{ 10, 10 };
	TankAttack* command = new TankAttack(tank, t, "fugas");

	command->Execute();

	delete tank;
}

void TestStatePattern()
{
	DragonEnemy dragon;
	dragon.SetMovementState(MovementState::WALK);
	dragon.Attack();

	dragon.SetMovementState(MovementState::FLY);
	dragon.Attack();
}

void TestObserverPattern()
{
	DayTimeSystem dt_sys;

	ITimeDependentActor* v = new Vampire();
	ITimeDependentActor* h = new Human();
	ITimeDependentActor* it_h = new ITHuman();


	dt_sys.Attach(v);
	dt_sys.Attach(h);
	dt_sys.Attach(it_h);

	dt_sys.Notify();

	std::this_thread::sleep_for(std::chrono::seconds(1));

	dt_sys.ChangeDayTime();

	dt_sys.Notify();

}

void TestVisitorPattern()
{
	Visitor* v = new Visitor();

	std::vector<Building*> places;
	places.emplace_back(new Cafe());
	places.emplace_back(new SportCentr());
	places.emplace_back(new Bank());
	places.emplace_back(new Cafe());

	for (const auto p : places)
	{
		p->Accept(v);
	}

 }

int main()
{
	//TestCommandPattern();
	//TestStatePattern();
	//TestObserverPattern();
	TestVisitorPattern();
}