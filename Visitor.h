#pragma once

class Visitor {
public:
	void Eat() const
	{
		std::cout << "Time to take some food\n";
	}

	void TakeSomeMoney() const
 	{
		std::cout << "Time to take some money for personal usage\n";
	}

	void DoingSport() const
	{
		std::cout << "Time for swimming\n";
	}
};

class Building {
public:
	virtual ~Building() {}
	virtual void Accept(Visitor* visitor) const = 0;
};


class Cafe : public Building
{
public:
	virtual void Accept(Visitor* v) const override
	{
		v->Eat();
	}
};

class SportCentr : public Building
{
public:
	virtual void Accept(Visitor* v) const override
	{
		v->DoingSport();
	}
};

class Bank : public Building
{
public:
	virtual void Accept(Visitor* v) const override
	{
		v->TakeSomeMoney();
	}
};