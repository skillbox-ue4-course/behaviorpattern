#pragma once
#include <string>

struct TargetPoint
{
	int x;
	int y;
};

class Command {
public:
	virtual ~Command() {
	}
	virtual void Execute() const = 0;
};

class Tank
{
public:
	void Move(const TargetPoint& p)
	{
		std::cout << "Moving to position (" << p.x << ", " << p.y << ")\n";
	}

	void Attack(const TargetPoint& p, const std::string& AmmoType)
	{
		std::cout << "Attack enemy at position (" << p.x << ", " << p.y << ")\n";
	}
};

class TankAttack : public Command {
private:
	Tank* m_Tank;
	const TargetPoint m_Target;
	const std::string m_AmmoType;
public:

	TankAttack(Tank* receiver, const TargetPoint& target, const std::string& ammo_type)
		: m_Tank(receiver), m_Target(target), m_AmmoType(ammo_type)
	{}

	void Execute() const override {
		std::cout << "Calculate distance between target and self location\n";
		std::cout << "Enemy is out of range, need moving\n";

		m_Tank->Move({ m_Target.x - 5, m_Target.y - 5 });

		std::cout << "Enemy in our range, applying shot\n";

		m_Tank->Attack(m_Target, m_AmmoType);
	}
};