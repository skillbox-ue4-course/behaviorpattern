#pragma once


enum class MovementState 
{
	WALK,
	FLY
};

class DragonEnemy
{
private:
	MovementState m_MovState = MovementState::WALK;

	void MelleAttack()
	{
		std::cout << "Use TailAttack\n";
	}

	void FlyAttack()
	{
		std::cout << "Casting FireBreath\n";
	}
public:
	MovementState GetMovementState() const
	{
		return m_MovState;
	}

	void SetMovementState(MovementState new_state)
	{
		m_MovState = new_state;
	}

	void Attack()
	{
		switch (m_MovState)
		{
		case MovementState::WALK:
			MelleAttack();
			break;
		case MovementState::FLY:
			FlyAttack();
			break;
		default:
			break;
		}
	}
};

