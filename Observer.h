#pragma once

#include <iostream>
#include <set>

enum class DayTime
{
	DAY,
	NIGHT,
};

class ITimeDependentActor
{
public:
	virtual ~ITimeDependentActor() {};
	virtual void Update(const DayTime newDayTime) = 0;
};


class DayTimeSystem
{
private:
	std::set<ITimeDependentActor*> m_Observers;
	DayTime m_CurrentDayTime = DayTime::DAY;

public:
	void Attach(ITimeDependentActor* observer)
	{
		if (observer)
		{
			m_Observers.insert(observer);
		}
	}
	void Detach(ITimeDependentActor* observer)
	{
		if (observer)
		{
			m_Observers.erase(observer);
		}
	}
	void Notify()
	{
		for (const auto s : m_Observers)
		{
			s->Update(m_CurrentDayTime);
		}
	}

	void ChangeDayTime()
	{
		m_CurrentDayTime = (m_CurrentDayTime == DayTime::DAY) ? DayTime::NIGHT : DayTime::DAY;
	}

};

class Vampire : public ITimeDependentActor
{
public:
	virtual void Update(const DayTime newDayTime)
	{
		if (newDayTime == DayTime::DAY) 
		{
			std::cout << "Vampire is going to bed. Sleep sugar\n";
		}
		else if (newDayTime == DayTime::NIGHT)
		{
			std::cout << "Vampire is going to hunt you down\n";
		}
	}
};

class Human : public ITimeDependentActor
{
public:
	virtual void Update(const DayTime newDayTime)
	{
		if (newDayTime == DayTime::DAY)
		{
			std::cout << "Human is going to work\n";
		}
		else if (newDayTime == DayTime::NIGHT)
		{
			std::cout << "Human is going to sleep\n";
		}
	}
};

class ITHuman : public ITimeDependentActor
{
public:
	virtual void Update(const DayTime newDayTime)
	{
		if (newDayTime == DayTime::DAY)
		{
			std::cout << "Ohh, i want to sleep, but i have to go work\n";
		}
		else if (newDayTime == DayTime::NIGHT)
		{
			std::cout << "Hell eaaah. It our time to krunch\n";
		}
	}
};